## Quiz testing

### Clone Project

### `git clone git@gitlab.com:Pawnpetcharat/quiz-game-testing.git`

or

### `https://gitlab.com/Pawnpetcharat/quiz-game-testing.git`

go to folder

### `cd quiz-game-testing`

### Installation

### `npm install`

Generate node module folder

### Run project

### `npm run start`

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.
